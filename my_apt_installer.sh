#!/bin/bash

set -eu -o pipefail # fail on erreor, debug all lines
sudo -n true
test $? -eq 0 || exit 1 "you should have sudo priviledge to run this script!"

echo installing the must-have pre-requisites
sleep 2

sudo apt-get update
sudo apt-get upgrade

while read -r p ;
    do echo installing $p
	sleep 2
	sudo apt-get install -y $p
	sleep 10
	clear ;

done < <(cat << "EOF"
    vim
	perl
	npm
	nodejs
	apache2
	python3
	chromium-browser
	mariadb-server
	phpmyadmin
	syslog-ng
EOF
)

sudo npm update npm -g
sudo npm cache clean -f
sudo npm install -g n
sudo n stable
sudo npm install -g forever-service
sudo npm install -g nodemon
sudo npm install -g syslog-client
sudo npm install -g winston
sudo pip install pip-download

sudo mysql_secure_installation